# Drones

## Build

1. For building the project execute:

```bash
mvn clean package
```

2. For building a docker image from source code:

```bash 
docker build -t t0lia/drones .
```

## Service health-check

Health-check accessible from: 'http://localhost:8080/actuator/health'

```bash
curl -X GET "http://localhost:8080/actuator/health" \
-H "accept: application/json"
```

## Service REST API

Service api you can find in api.http

## Running the application locally

For running the application execute:

```bash
docker-compose -f docker/docker-compose.yml up -d # start postgres database
mvn spring-boot:run # start application
```

## Running the application in docker

```bash
docker-compose build
docker-compose up 
```
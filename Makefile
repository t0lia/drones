OS=linux
ARCH=amd64

build-docker-image:
	docker build -t t0lia/drones .

up:
	docker-compose up -d

down:
	docker-compose down

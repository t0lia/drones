CREATE TABLE IF NOT EXISTS test.log
(
    id         integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    serial     text      NOT NULL,
    charge     integer   NOT NULL,
    status     text      NOT NULL,
    created    timestamp NOT NULL DEFAULT NOW(),
    medication TEXT
);
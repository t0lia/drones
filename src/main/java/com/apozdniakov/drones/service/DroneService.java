package com.apozdniakov.drones.service;

import com.apozdniakov.drones.domain.Drone;
import com.apozdniakov.drones.domain.Flight;
import com.apozdniakov.drones.domain.Medication;
import com.apozdniakov.drones.domain.DroneStatus;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class DroneService {

    public static final int DEFAULT_TRAVEL_PATH = 60; // 1 minute
    Logger logger = LoggerFactory.getLogger(DroneService.class);
    private final Map<String, Drone> drones;

    public Drone createDrone(Drone drone) {
        logger.info("Creating drone {}", drone);
        drones.put(drone.getModel(), drone);
        return drone;
    }

    public List<Drone> getAvailableDrones() {
        logger.info("Getting available drones");
        return drones.values().stream()
                .filter(drone -> drone.getState().getStatus() == DroneStatus.IDLE && drone.getState().getCharge() > 25)
                .collect(Collectors.toList());
    }

    public int getDroneCharge(String id) {
        logger.info("Checking charge for drone {}", id);
        return drones.get(id).getState().getCharge();
    }

    public boolean loadMedication(String id, Medication medication) {
        Drone drone = drones.get(id);
        if (drone.getWeightLimit() < medication.getWeight()) {
            logger.warn("Drone {} weight limit exceeded", id);
            return false;
        }
        if (drone.getState().getStatus() != DroneStatus.IDLE) {
            logger.warn("Drone {} isn't IDLE", id);
            return false;
        }
        logger.info("Loading medication {} to drone {}", medication.getName(), id);
        drone.getState().setStatus(DroneStatus.LOADING);
        drone.getState().setFlight(new Flight(medication, DEFAULT_TRAVEL_PATH));
        drone.getState().setStatus(DroneStatus.DELIVERING);
        return true;
    }

    public Medication checkMedication(String id) {
        logger.info("Checking medication for drone {}", id);
        Drone drone = drones.get(id);
        if (drone.getState().getFlight() == null) {
            logger.warn("Drone {} is not delivering any medication", id);
            return null;
        }
        return drone.getState().getFlight().getMedication();
    }
}

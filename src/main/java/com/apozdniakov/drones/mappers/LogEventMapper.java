package com.apozdniakov.drones.mappers;

import com.apozdniakov.drones.domain.LogEvent;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogEventMapper {

    @Insert("INSERT INTO log (serial, charge, status, created, medication) VALUES (#{serial}, #{charge}, #{status}, #{created}, #{medication})")
    void insertLogEvent(LogEvent entry);


}

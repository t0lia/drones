package com.apozdniakov.drones.repository;


import com.apozdniakov.drones.domain.Drone;
import com.apozdniakov.drones.domain.DroneState;
import com.apozdniakov.drones.domain.DroneStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Configuration
public class DroneRepositoryConfig {

    @Bean
    public Map<String, Drone> droneRepository() {
        return Stream.of(
                new Drone("R2-D2", "Lightweight", 100, 100,
                        new DroneState(75, DroneStatus.IDLE, null)),
                new Drone("T-1000", "Middleweight", 500, 100,
                        new DroneState(75, DroneStatus.IDLE, null)),
                new Drone("C3PO", "Heavyweight", 5000, 100,
                        new DroneState(75, DroneStatus.IDLE, null))
        ).collect(toMap(Drone::getSerialNumber, identity()));
    }


}

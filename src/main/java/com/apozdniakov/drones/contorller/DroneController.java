package com.apozdniakov.drones.contorller;

import com.apozdniakov.drones.domain.DroneState;
import com.apozdniakov.drones.domain.Drone;
import com.apozdniakov.drones.domain.Medication;
import com.apozdniakov.drones.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("drone")
public class DroneController {

    private final DroneService droneService;

    /**
     * Registering a drone
     *
     * @param drone - drone to register
     */
    @PostMapping
    public Drone createDrone(@RequestBody Drone drone) {
        return droneService.createDrone(drone);
    }

    /**
     * Checking available drones for loading
     *
     * @return list of available drones
     */
    @GetMapping
    public List<Drone> getAvailableDrones() {
        return droneService.getAvailableDrones();
    }

    /**
     * Check drone battery level for a given drone
     *
     * @param id drone id
     * @return battery level
     */
    @GetMapping("{id}/charge")
    public int getDroneCharge(@PathVariable("id") String id) {
        return droneService.getDroneCharge(id);
    }


    /**
     * Loading a drone with medication items
     *
     * @param medication medication
     */
    @PostMapping("{id}/medication")
    public ResponseEntity loadMedication(@PathVariable("id") String id, @RequestBody Medication medication) {
        return droneService.loadMedication(id, medication) ?
                ResponseEntity.ok().build() :
                ResponseEntity.badRequest().build();
    }


    /**
     * Checking loaded medication items for a given drone
     *
     * @param id drone id
     * @return medication
     */
    @GetMapping("{id}/medication")
    public ResponseEntity<Medication> checkMedication(@PathVariable("id") String id) {
        Medication medication = droneService.checkMedication(id);
        return medication != null ? ResponseEntity.ok(medication) : ResponseEntity.badRequest().build();
    }
}

package com.apozdniakov.drones.domain;

public enum DroneStatus {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
}

package com.apozdniakov.drones.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Medication {
    //- name (allowed only letters, numbers, ‘-‘, ‘_’);
    private String name;
    //- weight;
    private int weight;
    //- code (allowed only upper case letters, underscore and numbers);
    private String code;
    //- image (picture of the medication case).
    private byte[] image;
}

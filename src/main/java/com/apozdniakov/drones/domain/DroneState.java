package com.apozdniakov.drones.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DroneState {
    // battery charge from 0 to 100
    private int charge;
    // status
    private DroneStatus status;
    // flight (if the drone is in the air, otherwise null).
    private Flight flight;
}

package com.apozdniakov.drones.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class LogEvent {
    public LogEvent(Drone drone) {
        this.serial = drone.getSerialNumber();
        this.charge = drone.getState().getCharge();
        this.status = drone.getState().getStatus().toString();
        this.created = LocalDateTime.now();
        Flight flight = drone.getState().getFlight();
        this.medication = flight != null ? flight.getMedication().getName() : null;
    }

    private int id;
    private String serial;
    private int charge;
    private String status;
    private LocalDateTime created;
    private String medication;
}

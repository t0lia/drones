package com.apozdniakov.drones.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Flight {
    private Medication medication;
    private int timeLeft;
}

package com.apozdniakov.drones.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Drone {

    // serial number (100 characters max);
    private String serialNumber;
    // model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
    private String model;
    // weight limit (500gr max);
    private int weightLimit;
    // battery capacity (percentage);
    private int batteryCapacity;
    // state
    private DroneState state;

}

package com.apozdniakov.drones.time;

import com.apozdniakov.drones.domain.Drone;
import com.apozdniakov.drones.domain.Flight;
import com.apozdniakov.drones.domain.LogEvent;
import com.apozdniakov.drones.mappers.LogEventMapper;
import com.apozdniakov.drones.domain.DroneStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class SpaceTimeEvaluator {


    private final Map<String, Drone> drones;
    private final LogEventMapper logger;

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() {
        drones.values().forEach(drone -> {
            Flight flight = drone.getState().getFlight();
            if (flight != null) {
                flight.setTimeLeft(flight.getTimeLeft() - 1);
                if (flight.getTimeLeft() <= 0) {
                    finishFlight(drone);
                }
            }

            if (drone.getState().getStatus() == DroneStatus.IDLE && drone.getState().getCharge() < 100) {
                drone.getState().setCharge(drone.getState().getCharge() + 1);
            }
            if (drone.getState().getStatus() == DroneStatus.DELIVERING) {
                drone.getState().setCharge(drone.getState().getCharge() - 1);
            }
            logger.insertLogEvent(new LogEvent(drone));
        });
    }

    public void finishFlight(Drone drone) {
        drone.getState().setStatus(DroneStatus.DELIVERED);
        drone.getState().setFlight(null);
        drone.getState().setStatus(DroneStatus.IDLE);
    }
}

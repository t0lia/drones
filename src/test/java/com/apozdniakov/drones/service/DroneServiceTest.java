package com.apozdniakov.drones.service;

import com.apozdniakov.drones.domain.Drone;
import com.apozdniakov.drones.domain.DroneState;
import com.apozdniakov.drones.domain.Medication;
import com.apozdniakov.drones.domain.DroneStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

class DroneServiceTest {

    @Test
    void checkAvailableDrones() {
        Map<String, Drone> drones = Stream.of(
                new Drone("R2-D2", "Lightweight", 100, 100,
                        new DroneState(24, DroneStatus.IDLE, null)),
                new Drone("T-1000", "Middleweight", 500, 100,
                        new DroneState(100, DroneStatus.IDLE, null)),
                new Drone("C3PO", "Heavyweight", 5000, 100,
                        new DroneState(100, DroneStatus.LOADING, null))
        ).collect(toMap(Drone::getSerialNumber, identity()));
        DroneService droneService = new DroneService(drones);

        List<Drone> availableDrones = droneService.getAvailableDrones();
        Assertions.assertEquals(1, availableDrones.size());
        Drone drone = availableDrones.get(0);
        Assertions.assertEquals(drone.getSerialNumber(), "T-1000");

    }

    @Test
    void testLoadMedication() {
        Map<String, Drone> drones = Stream.of(
                new Drone("R2-D2", "Lightweight", 100, 100,
                        new DroneState(100, DroneStatus.IDLE, null)),
                new Drone("T-1000", "Middleweight", 500, 100,
                        new DroneState(100, DroneStatus.IDLE, null)),
                new Drone("C3PO", "Heavyweight", 5000, 100,
                        new DroneState(100, DroneStatus.IDLE, null))
        ).collect(toMap(Drone::getSerialNumber, identity()));
        DroneService droneService = new DroneService(drones);

        droneService.loadMedication("R2-D2", new Medication("Penicillin", 700, "pnc", null));
        Assertions.assertEquals(DroneStatus.IDLE, drones.get("R2-D2").getState().getStatus());

        droneService.loadMedication("C3PO", new Medication("Penicillin", 700, "pnc", null));
        Assertions.assertEquals(DroneStatus.DELIVERING, drones.get("C3PO").getState().getStatus());

    }

    @Test
    void testCreateDrone() {
        Map<String, Drone> drones = Stream.of(
                new Drone("Lightweight", "R2-D2", 100, 100,
                        new DroneState(100, DroneStatus.IDLE, null)),
                new Drone("Middleweight", "T-1000", 500, 100,
                        new DroneState(100, DroneStatus.IDLE, null))
        ).collect(toMap(Drone::getSerialNumber, identity()));
        DroneService droneService = new DroneService(drones);

        Drone drone = new Drone("Heavyweight", "C3PO", 5000, 100,
                new DroneState(100, DroneStatus.IDLE, null));
        droneService.createDrone(drone);
        Assertions.assertEquals(3, drones.size());
        Assertions.assertEquals(drone, drones.get("C3PO"));
    }


}